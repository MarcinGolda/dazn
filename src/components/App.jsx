import React from 'react';

import MovieList from './MovieList';
import SerachBar from './SearchBar';
import UrlHelper from '../utils/urlHelper';
import constants from '../utils/constants';
import debounce from '../utils/debounce';

import '../style/index.scss'


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            movies: [],
            page: 1,
            totalPages: 1,
            query: '',
            isError: false,
            isCallPending: false,
            errorMsg: 'ups! We could not fetch movies.',
            noResultMsg: 'No results were found. Please type different phrase.',
            searchBarTitle: 'Search movie'
        };

        this.dbConnector = this.props.dbConnector;
        this.urlHelper = UrlHelper;

        this.fetchMovies = this.fetchMovies.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.debounceFetchMovies = debounce(this.fetchMovies);
    };

    handleSearch(query) {
        this.setState({
            query,
            movies: [],
            page: 1,
            totalPages: 1,
            isError: false,
            isCallPending: query.length > 0
        }, () => this.debounceFetchMovies());
    };

    fetchMovies() {
        const { movies, page, totalPages, query } = this.state;
        
        if (page > totalPages || query.length === 0) {
            return;
        }

        const { dbUrl, apiKey } = constants;
        const url = this.urlHelper.createUrl(dbUrl, { api_key: apiKey, query, page });

        this.setState({ isCallPending: true });

        this.dbConnector.get(url)
            .then(res => {
                this.setState({ 
                    movies: movies.length === 0 ? res.results : [...movies, ...res.results],
                    page: page + 1,
                    totalPages: res.total_pages,
                    isError: false,
                    isCallPending: false
                });
            })
            .catch(() => this.setState({ isError: true, isCallPending: false }));
    };

    renderNoItemsPlaceholder() {
        const { errorMsg, isError, noResultMsg } = this.state;
        return <h2 className='no-items-placholder'>{ isError ? errorMsg : noResultMsg }</h2>;
    };

    renderAppContent() {
        const { isCallPending, movies, searchBarTitle, query } = this.state;
        const noItems = movies.length === 0;

        return (
            <div className='app-wrapper'>
                <SerachBar 
                    title={searchBarTitle}
                    handleSearch={this.handleSearch}
                    isCallPending={isCallPending}
                    query={query} />

                {
                    noItems
                        ? this.renderNoItemsPlaceholder()
                        : <MovieList movies={movies} fetchMovies={this.fetchMovies} />
                }
            </div>
        );
    };

    render() {
        return this.renderAppContent();
    };
};

export default App;