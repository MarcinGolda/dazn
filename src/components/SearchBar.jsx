import React from 'react';
import PropTypes from 'prop-types';


const SearchBar = ({ isCallPending, handleSearch, title, query }) => {
    const handleChange = (event) => {
        handleSearch(event.target.value);
    };

    return (
        <div className='search-bar-wrapper'>
            <h3 className='search-bar-title'>{title}</h3>
            <input
                className='search-bar-input'
                type='text'
                value={query}
                onChange={handleChange} />
            <h3 className='loading-placeholder'>{isCallPending && '...Loading'}</h3>
        </div>
    );
};

SearchBar.propTypes = {
    handleSearch: PropTypes.func,
    query: PropTypes.string
};

export default SearchBar;