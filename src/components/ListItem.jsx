import React from 'react';
import PropTypes from 'prop-types';

import constants from '../utils/constants';

const ListItem = ({ movie }) => {
    return (
        <li className='movie-item'>
            <div className='movie-item-header'>
                {
                    movie.poster_path && 
                    <img 
                        className='movie-item-img'
                        src={`${constants.imageUrl}${movie.poster_path}`} 
                        alt='movie image' />
                }
                
                <h3 className='movie-item-title'>{ movie.title ? movie.title : 'Sorry. No title provided.'}</h3>
            </div>
            
            <p className='movie-item-decription'>{ movie.overview ? movie.overview : 'Movie is waiting for it\'s description.' }</p>
        </li>
    );
};

ListItem.propTypes = {
    movie: PropTypes.object
};

export default ListItem;