import React from 'react';
import PropTypes from 'prop-types';

import ListItem from './ListItem';
import lazyLoading from '../utils/lazyLoading';


class MovieList extends React.Component {
    constructor(props) {
        super(props);

        this.lazyLoading = lazyLoading;
        this.handleLazyLoading = this.handleLazyLoading.bind(this);
    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleLazyLoading, false);
    };

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLazyLoading, false);
    };

    handleLazyLoading() {
        return this.lazyLoading('app', this.props.fetchMovies);
    };

    renderItemList() {
        return this.props.movies.map((movie, idx) => {
            return <ListItem key={idx} movie={movie} />
        });
    };

    render() {
        return(
            <ul className='movie-list-wrapper'>
                { this.renderItemList() }
            </ul>
        );
    };
};

MovieList.propTypes = {
    fetchMovies: PropTypes.func,
    movies: PropTypes.array
};

export default MovieList;