class DBconnector {
    /**
     * Generic call method.
     * @param {string} method HTTP method to use (GET, PUT, POST, DELETE etc)
     * @param {string} url Url to call
     * @param {Object} body Body to use in call. Remember that body in GET is not widely supported.
     * @param {Object} headers Headers object, like in [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers).
    */
    _call(method, url, body, headers) {
        return this._handleCall(method, url, body, headers);
    }

    /**
     *  API Get method
     * @param {string} url Url to call
     * @param {Object} [headers] Headers object, like in [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers).
    */
    get(url = '', headers = {}) {
        return this._call('get', url, null, headers);
    }

    /**
     * API Post method
     * @param {string} url Url to call
     * @param {Object} body Body to use in call.
     * @param {Object} [headers] Headers object, like in [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers).
     */
    post(url = '', body = {}, headers = {}) {
        return this._call('post', url, body, headers);
    }

    /**
     * API Put method
     * @param {string} url Url to call
     * @param {Object} body Body to use in call.
     * @param {Object} [headers] Headers object, like in [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers).
     */
    put(url = '', body = {}, headers = {}) {
        return this._call('put', url, body, headers);
    }

    /**
     * API Delete method
     * @param {string} url Url to call
     * @param {Object} body Body to use in call.
     * @param {Object} [headers] Headers object, like in [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers).
     */
    delete(url, body, headers = null) {
        return this._call('delete', url, body, headers);
    }

    _handleCall(method, url, body, headers) {
        return new Promise((resolve, reject) => {
            const requestParams = method === 'get' ? { method, headers } : { method, body, headers };

            fetch(url, requestParams)
                .then(res => {
                    if (res.status >= 200 && res.status < 300) {
                        return resolve(res.json());
                    } else {
                        return reject(res.json());
                    }
                })
                .catch(err => reject(err));
        });
    }
};

export default DBconnector;