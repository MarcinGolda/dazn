/**
 * Function for handling debouncing of search query
 * @param {function} callback Function which will be called after delay
 * @param {number} delay time in miliseconds
*/
const debounce = (callback, delay = 1000) => {
    let inDebounce;

    return () => {
        const context = this;
        const args = arguments;
        
        clearTimeout(inDebounce);
        inDebounce = setTimeout(() => callback.apply(context, args), delay);
    };
};

export default debounce;