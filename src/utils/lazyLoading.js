/**
 * Function for handling lazy loading when user scroll to the bottom of screen
 * @param {string} elementId Id of HTML element
 * @param {function} callback Function which will be called when condition is met
 * @param {array} params Params which are passed to callback
*/
const lazyLoading = (elementId, callback, ...params) => {
    const app = document.getElementById(elementId);
    const contentHeight = app.offsetHeight;
    const yOffset = window.pageYOffset;
    const y = yOffset + window.innerHeight;

    if (y >= contentHeight) {
        callback(...params);
    }
};

export default lazyLoading;