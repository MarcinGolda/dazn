class UrlHelper {
    /**
     * Generic method for creating url with queryParams
     * @param {string} url Url to call
     * @param {Object} queryParams Params to be added to url
    */
    static createUrl(url, ...queryParams) {
        return `${url}?${this.serializeToUrlQuery(...queryParams)}`;
    };

    /**
     * Generic method for creating queryParams from object
     * @param {Object} queryParams Params to be serialized
    */
    static serializeToUrlQuery(queryParams) {
        return Object.keys(queryParams).map(k => `${k}=${queryParams[k]}`).join('&');
    };

    /**
     * Helper method for getting queryString parameters from url.
     * @param {string} url Url to mock
     */
    static splitQueryParamsFromUrl(url) {
        if (url.split('?').length === 1) {
            return {
                urlAddress: url,
                requestData: ''
            };
        }

        const [urlAddress, params] = url.split('?');

        const queryParams = params
            .split('&')
            .map(item => {
                const [k, v] = item.split('=');
                return { [k]: v }
            });

        return {
            urlAddress,
            requestData: Object.assign({}, ...queryParams)
        };
    };
};

export default UrlHelper;