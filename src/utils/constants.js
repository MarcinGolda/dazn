const constants = {
    apiKey: "",
    dbUrl: "https://api.themoviedb.org/3/search/movie",
    imageUrl: "https://image.tmdb.org/t/p/w185"
};

export default constants;