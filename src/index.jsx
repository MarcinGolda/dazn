import '@babel/polyfill';

import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import DBconnector from './services/dbConnector';

const dbConnector = new DBconnector();

render(<App dbConnector={dbConnector} />, document.getElementById('app'));