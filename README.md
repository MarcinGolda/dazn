# DAZN recruitment task

This repo contains DAZN recruitment task.
It is a simple application for browsing movies.

## Run app

To run the application localy, use:

 - create your API_KEY according to (https://developers.themoviedb.org/3/getting-started/introduction)
 - add yout key to src/utils/constants
 - npm install
 - npx webpack
 - npm start

## Testing

To run automated tests run `npm test`

## Linting

To run eslint run `npm run lint`

## Documentation

For external libraries, see list below:
  - [Webpack](https://webpack.js.org/)
  - [React](https://facebook.github.io/react/),
  - [Jest](https://jestjs.io/docs/en/getting-started.html)

## Movie DB

Application is working with [Movie DB](https://developers.themoviedb.org/3/getting-started/introduction)
