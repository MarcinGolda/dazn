import UrlHelper from '../src/utils/urlHelper';

describe('UrlHelper', () => {
    it('tests "createUrl" method', () => {
        const url = UrlHelper.createUrl('https://test.com', { name: 'test', age: 10 });

        expect(url).toBe('https://test.com?name=test&age=10');
    });

    it('tests "serializeToUrlQuery" method', () => {
        const queryParams = UrlHelper.serializeToUrlQuery({ name: 'test', age: 10 });

        expect(queryParams).toBe('name=test&age=10');
    });

    it('tests "splitQueryParamsFromUrl" method', () => {
        const {
            urlAddress,
            requestData
        } = UrlHelper.splitQueryParamsFromUrl('https://test.com?name=test&age=10');

        expect(urlAddress).toBe('https://test.com');
        expect(requestData).toMatchObject({ name: 'test', age: "10" });
    });
});