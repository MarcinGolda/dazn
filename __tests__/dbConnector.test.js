import DBconnector from '../src/services/dbConnector';

describe('DBconnector', () => {
    it('test response status 200', () => {
        window.fetch = (url, {method, body, headers}) => Promise.resolve({status: 200, body: {test: 1}, json: () => ({test: 1})});

        const dbConnector = new DBconnector();

        return dbConnector.get('testUri')
            .then(reponse => expect(reponse).toMatchObject({test: 1}));
    });

    it('test response status 404', () => {
        window.fetch = (url, {method, body, headers}) => Promise.resolve({status: 404, body: {Error: 'Not found'}, json: () => ({Error: 'Not found'})});

        const dbConnector = new DBconnector();

        return dbConnector.get('testUri')
            .catch(reponse => expect(reponse).toMatchObject({Error: 'Not found'}));
    });

    it('test connection error', () => {
        window.fetch = (url, {method, body, headers}) => Promise.reject('Error');

        const dbConnector = new DBconnector();

        return dbConnector.get('testUri')
            .catch(reponse => expect(reponse).toBe('Error'));
    });
});